#!/bin/sh

# reset config files
sudo git checkout -- sentinel-*

# reset the owner settings
sudo chown -R 1001:1001 sentinel-*

