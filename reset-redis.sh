#!/bin/sh

# reset config files
sudo git checkout -- redis-*

# reset the owner settings
sudo chown -R 999:999 redis-*

