#!/bin/sh
echo "sentinel-1:"
docker-compose exec sentinel-1 redis-cli -p 5001 -a dont_hack_anything info sentinel
echo
echo "sentinel-2:"
docker-compose exec sentinel-2 redis-cli -p 5002 -a dont_hack_anything info sentinel
echo
echo "sentinel-3:"
docker-compose exec sentinel-3 redis-cli -p 5003 -a dont_hack_anything info sentinel

