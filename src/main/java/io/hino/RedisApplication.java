package io.hino;

import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisNode;
import org.springframework.data.redis.connection.RedisSentinelConfiguration;
import org.springframework.data.redis.connection.RedisSentinelConnection;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.types.RedisClientInfo;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@SpringBootApplication
public class RedisApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(RedisApplication.class);

    private static final char SEPARATOR = ',';

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Bean
    public CommandLineRunner getClientsInfo(ApplicationContext ctx) {
        return args -> {
            List<RedisClientInfo> redisClients = Objects.requireNonNull(redisTemplate.getClientList());
            LOGGER.info("client list: {}", redisClients.stream().map(RedisClientInfo::getName).collect(Collectors.toList()));
            LOGGER.info("address list: {}", redisClients.stream().map(RedisClientInfo::getAddressPort).collect(Collectors.toList()));
            LOGGER.info("lass command: {}", redisClients.stream().map(RedisClientInfo::getLastCommand).collect(Collectors.toList()));
        };
    }

    @Bean
    public CommandLineRunner verifyRedisOperations(ApplicationContext ctx) {
        return args -> {
            Set<String> keys = redisTemplate.keys("*");
            Objects.requireNonNull(keys).forEach(key -> {
                LOGGER.info("data: [{} = {}]", key, redisTemplate.opsForValue().get(key));
            });
            String uuid = UUID.randomUUID().toString();
            redisTemplate.opsForValue().set("uuid", uuid);
            LOGGER.info("Assertion with set: [expected = {}]", uuid);
            LOGGER.info("Assertion with set: [actual   = {}]", redisTemplate.opsForValue().get("uuid"));
        };
    }

    public static void main(String[] args) {

        ApplicationContext ctx = SpringApplication.run(RedisApplication.class, args);

        RedisSentinelConfiguration sentinelConfiguration = Objects.requireNonNull(
                ctx.getBean(LettuceConnectionFactory.class).getSentinelConfiguration());
        RedisSentinelConnection sentinelConnection = ctx.getBean(RedisConnectionFactory.class).getSentinelConnection();

        LOGGER.info("redis sentinels: {}", Strings.join(sentinelConfiguration.getSentinels()
                .stream().map(RedisNode::asString)
                .collect(Collectors.toList()), SEPARATOR));

        sentinelConnection.masters().forEach(redisServer -> {
            LOGGER.info("redis master: {}", redisServer.asString());
            LOGGER.info("redis replica: {}",
                    Strings.join(sentinelConnection.slaves(redisServer)
                            .stream().map(RedisNode::asString)
                            .collect(Collectors.toList()), SEPARATOR));
            sentinelConnection.slaves(redisServer).forEach(RedisNode::asString);
        });
    }
}
