package io.hino.controller;

import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisNode;
import org.springframework.data.redis.connection.RedisSentinelConnection;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/")
public class RedisController {

    private static final String NEW_LINE = System.getProperty("line.separator");

    private static final char SEPARATOR = ',';

    @Autowired
    private LettuceConnectionFactory lettuceConnectionFactory;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @RequestMapping("/info")
    public String getRedisInfo() {

        StringBuilder result = new StringBuilder();
        RedisSentinelConnection connection = Objects.requireNonNull(lettuceConnectionFactory).getSentinelConnection();

        result.append("redis sentinels: ").append(
                Strings.join(Objects.requireNonNull(lettuceConnectionFactory.getSentinelConfiguration()).getSentinels()
                        .stream().map(RedisNode::asString)
                        .collect(Collectors.toList()), SEPARATOR)).append(NEW_LINE);

        connection.masters().forEach(redisServer -> {
            result.append("redis master: ").append(redisServer.asString()).append(NEW_LINE);
            result.append("redis replica: ").append(
                    Strings.join(connection.slaves(redisServer)
                            .stream().map(RedisNode::asString)
                            .collect(Collectors.toList()), SEPARATOR));
            connection.slaves(redisServer).forEach(RedisNode::asString);
        });
        return result.toString();
    }

    @GetMapping("keys")
    public String get(@RequestParam(required = false) String key) {
        StringBuilder result = new StringBuilder();

        // list all the key-value pairs
        if (Strings.isBlank(key)) {
            Set<String> keys = redisTemplate.keys("*");
            Objects.requireNonNull(keys).forEach(k -> {
                result.append(String.format("data: [%s = %s]", k, redisTemplate.opsForValue().get(k))).append(NEW_LINE);
            });
        } else {
            result.append(redisTemplate.opsForValue().get(key));
        }
        return result.toString();
    }

    @PutMapping("keys")
    public String update(@RequestParam String key, @RequestParam String value) {
        redisTemplate.opsForValue().set(key, value);
        return redisTemplate.opsForValue().get(key);
    }
}
